package programUtilities;

import java.util.Scanner;

public class InputController {

  private Scanner scanner;
  private static InputController instance;

  private InputController() {
    scanner = new Scanner(System.in);
  }

  public static synchronized InputController getInstance() {
    if (instance == null) {
      instance = new InputController();
    }
    return instance;
  }

  public Integer getInt() {
    while (!scanner.hasNextInt()) {
      System.out.println("Wrong type, integer is needed!");
      scanner.next();
    }
    return scanner.nextInt();
  }

  public Double getDouble() {
    while (!scanner.hasNextDouble()) {
      System.out.println("Wrong type, double is needed!");
      scanner.next();
    }
    return scanner.nextDouble();
  }

  public String getStringNext() {
    return scanner.next();
  }
}
