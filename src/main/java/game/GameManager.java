package game;

import static models.Color.RED;
import static models.Color.WHITE;

import models.BattleDroid;
import models.Droid;
import models.Player;
import models.Arena;
import models.RepairDroid;
import programUtilities.InputController;

public class GameManager {

  private Arena arena = Arena.getInstance();
  private InputController ic = InputController.getInstance();
  private Player currentPlayer = new Player(RED, "Player № 1");
  private Player oppositePlayer = new Player(WHITE, "Player № 2");
  private Droid currentDroid;
  private Integer currentX;
  private Integer currentY;
  private String currentCommand;
  private Integer roundsCount = 0;

  /*
   * Round means the change of the player, who is making the move right now
   */
  public void nextRound() {
    while (!isGameOver()) {
      System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
      System.out.println("Round № " + (roundsCount + 1));
      System.out.println(
          "Player " + currentPlayer.getName() + ". Color: " + currentPlayer.getColor().toString());
      nextMove();
      roundsCount++;
      switchPlayers();
    }
    if (arena.getRedTeam() <= 0) {
      System.out.println("White team won the game!");
    } else if (arena.getWhiteTeam() <= 0) {
      System.out.println("Red team won the game!");
    }
  }

  /*
   * nextMove has all the operations current player should do
   */
  private void nextMove() {
    arena.ShowArena();
    chooseDroidToPlay();
    do {
      chooseCommand();
    } while (!isCommandSuccessful());
    arena.randomBuff();
  }

  /*
   * Allows to pick up droids from current player's team
   */
  private void chooseDroidToPlay() {
    System.out.println("Which droid would you like to choose?");
    chooseNewCoordinates();
    while (arena.isCordEmpty(currentX, currentY)
        || !doesDroidBelongToPlayer(currentX, currentY)) {
      System.out.println("The cell is empty or the droid is not yours.");
      chooseNewCoordinates();
    }
    currentDroid = Arena.getInstance().returnDroidOnCell(currentX, currentY);
    currentDroid.getInfoAboutDroid();
  }

  /*
   * Picks up new coordinates, if mistaken previously
   */
  private void chooseNewCoordinates() {
    System.out.println("Enter coordinates (X and Y)");
    while (!(isCoordInTheBounds(currentX = ic.getInt())
        && isCoordInTheBounds(currentY = ic.getInt()))) {
      System.out.println("Coordinates are out of bounds from 0 to 50. Please, try again");
    }
  }

  /*
   * Checks, if player wasn't mistaken during executing commands
   */
  private Boolean isCommandSuccessful() {
    if (currentCommand.equals("s")) {
      return didDroidShoot();
    } else if (currentCommand.equals("w")) {
      return didDroidMove();
    } else if (currentCommand.equals("h")) {
      return didDroidHeal();
    } else {
      return false;
    }
  }

  /*
   * Current player decides. what to do: to walk, to shoot or to heal
   */
  private void chooseCommand() {
    System.out.println("What would you like to do? (walk = w, shoot = s, heal = h)");
    while (!(isTheLetterCorrect(currentCommand = ic.getStringNext()))) {
      System.out.println("Please, enter only w, s or h. Please, try again.");
    }
  }

  /*
   * Checks if walking operation was successful
   */
  private Boolean didDroidMove() {
    chooseNewCoordinates();
    while (!currentDroid.walk(currentX, currentY)) {
      System.out.println("You walked nowhere, try again");
      return false;
    }
    return true;
  }

  /*
   * Checks if shooting operation was successful
   */
  private Boolean didDroidShoot() {
    while ((!(currentDroid.getClass() == BattleDroid.class)) && (currentCommand.equals("s"))) {
      System.out.println("Chosen droid cannot do this. Please, try another command.");
      return false;
    }
    chooseNewCoordinates();
    while (!(((BattleDroid) currentDroid).shoot(currentX, currentY))) {
      System.out.println("You shooted nothing, try again");
      return false;
    }
    return true;
  }

  /*
   * Checks if healing operation was successful
   */
  private Boolean didDroidHeal() {
    while ((!(currentDroid.getClass() == RepairDroid.class)) && (currentCommand.equals("h"))) {
      System.out.println("Chosen droid cannot do this. Please, try another command.");
      return false;
    }
    chooseNewCoordinates();
    while (!(((RepairDroid) currentDroid).heal(currentX, currentY))) {
      System.out.println("You healed nothing, try again");
      return false;
    }
    return true;
  }

  /*
   * Checks if picked up coordinates are not out of bounds
   */
  private Boolean isCoordInTheBounds(Integer input) {
    return (input >= 0 && input < 50);
  }

  /*
   * Checks if currentComand is s, w or h
   */
  private Boolean isTheLetterCorrect(String input) {
    return (input.equals("s") || input.equals("h") || input.equals("w"));
  }

  /*
   * Checks if the picked up droid belong to player
   */
  private Boolean doesDroidBelongToPlayer(Integer x, Integer y) {
    if (arena.returnDroidOnCell(x, y) == null) {
      return false;
    }
    return arena.returnDroidOnCell(x, y).getColor() == currentPlayer.getColor();
  }

  /*
   * Checks if there is no droids in one of the teams
   */
  public Boolean isGameOver() {
    return arena.isOneOfTeamsDead();
  }

  /*
   * Switches players at the end of the round
   */
  private void switchPlayers() {
    Player tmp = currentPlayer;
    currentPlayer = oppositePlayer;
    oppositePlayer = tmp;
  }
}
