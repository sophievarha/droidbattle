package models;

import java.util.Random;

public class Arena {

  private Object[][] map; // Map for droid objects. Arena size = 10x10
  private static Arena instance; //Singleton
  private int whiteTeam = 5;
  private int redTeam = 5;


  public int getWhiteTeam() {
    return whiteTeam;
  }

  public void setWhiteTeam(int whiteTeam) {
    this.whiteTeam = whiteTeam;
  }

  public void setWhiteTeamDeadDroid() {
    this.whiteTeam--;
  }

  public int getRedTeam() {
    return redTeam;
  }

  public void setRedTeam(int redTeam) {
    this.redTeam = redTeam;
  }

  public void setBlackTeamDeadDroid() {
    this.redTeam--;
  }


  public void ShowArena() {

    final String ANSI_RESET = "\u001B[0m";
    final String ANSI_BLACK = "\u001B[30m";
    final String ANSI_RED = "\u001B[31m";
    final String ANSI_GREEN = "\u001B[32m";
    final String ANSI_YELLOW = "\u001B[33m";
    final String ANSI_BLUE = "\u001B[34m";
    final String ANSI_PURPLE = "\u001B[35m";
    final String ANSI_CYAN = "\u001B[36m";
    final String ANSI_WHITE = "\u001B[37m";

    System.out.println(ANSI_YELLOW + "\t##########RED###########" + ANSI_RESET);
    int k = 9;
    for (int i = 9; i >= 0; i--) {
      System.out.print(ANSI_YELLOW + "\t#" + ANSI_PURPLE + k-- + ANSI_RESET);
      System.out.print(ANSI_YELLOW + "|" + ANSI_RESET);
      for (int j = 0; j < 10; j++) {
        if (map[j][i] != null) {
          if ((map[j][i] != null) && (map[j][i].getClass() == ImmediateDeath.class)) {
            System.out.print(ANSI_BLUE + "?" + ANSI_RESET);
          } else {
            if (returnDroidOnCell(j, i).getColor().equals(Color.WHITE)) {
              if (returnDroidOnCell(j, i).getClass() == RepairDroid.class) {
                System.out.print(ANSI_BLACK + "+" + ANSI_RESET);
              } else {
                System.out.print(ANSI_BLACK + "x" + ANSI_RESET);
              }
            } else {
              if (returnDroidOnCell(j, i).getColor().equals(Color.RED)) {
                if (returnDroidOnCell(j, i).getClass() == RepairDroid.class) {
                  System.out.print(ANSI_RED + "+" + ANSI_RESET);
                } else {
                  System.out.print(ANSI_RED + "x" + ANSI_RESET);
                }
              }
//              System.out.print(ANSI_RED + "x" + ANSI_RESET);
            }
          }
        } else {
          System.out.print(ANSI_BLUE + "_" + ANSI_RESET);
        }
        System.out.print(ANSI_BLUE + "|" + ANSI_RESET);
      }
      System.out.println(ANSI_YELLOW + "#" + ANSI_RESET);
      if (i == 0) {
        System.out.println(
            ANSI_YELLOW + "\t#" + ANSI_PURPLE + "  0|1|2|3|4|5|6|7|8|9|" + ANSI_YELLOW + "#"
                + ANSI_RESET);
        System.out.println(ANSI_YELLOW + "\t#########WHITE##########" + ANSI_RESET);
      }
    }
  }

  private Arena() {
    map = new Object[10][10];

    {     /** Generating Red(top) team */
      map[1][9] = new BattleDroid(1, 9, Color.RED, 1);
      map[3][9] = new BattleDroid(3, 9, Color.RED, 2);
      map[5][9] = new RepairDroid(5, 9, Color.RED, 3);
      map[7][9] = new RepairDroid(7, 9, Color.RED, 4);
      map[9][9] = new BattleDroid(9, 9, Color.RED, 5);
    }
    {     /** Generating White(bot) team */

      map[0][0] = new BattleDroid(0, 0, Color.WHITE, 6);
      map[2][0] = new BattleDroid(2, 0, Color.WHITE, 7);
      map[4][0] = new RepairDroid(4, 0, Color.WHITE, 8);
      map[6][0] = new RepairDroid(6, 0, Color.WHITE, 9);
      map[8][0] = new BattleDroid(8, 0, Color.WHITE, 10);
    }
  }

  public static Arena getInstance() {         // Singleton creating Arena
    if (instance == null) {
      instance = new Arena();
      System.out.println("Arena generated");
    }
    return instance;
  }

  public boolean isCordEmpty(int currentX, int currentY) {  // Check for free Cords on map
    if (map[currentX][currentY] == null || map[currentX][currentY].getClass() == Buff.class) {
      return true;
    }
    return false;
  }

  public boolean moveTo(int curX, int curY, int newX,       /** Moving droid on map */
      int newY) {
    if (isCordEmpty(newX, newY)) {
      Object temp = map[curX][curY];
      map[curX][curY] = null;
      map[newX][newY] = temp;
      return true;
    }
    return false;
  }

  public void deleteDroid(int curX, int curY) {     /** Deleting droid from map */
    Droid droid = (Droid) map[curX][curY];
    if (droid.getColor() == Color.WHITE) {
      setWhiteTeamDeadDroid();
    } else if (droid.getColor() == Color.RED) {
      setBlackTeamDeadDroid();
    }
    map[curX][curY] = null;
  }

  public void deleteBuff(int curX, int curY) {
    map[curX][curY] = null;
  }

  public boolean isOneOfTeamsDead() {               /** Check for number of alive droids in both teams */
    return whiteTeam <= 0 || redTeam <= 0;
  }

  public void randomBuff() {
    Integer rndX;
    Integer rndY;
    if (isRandomBuffPossible()) {
      Random rnd = new Random();
      do {
        rndX = rnd.nextInt(9) + 1;
        rndY = rnd.nextInt(9) + 1;
      } while (!isCordEmpty(rndX, rndY));
      map[rndX][rndY] = new ImmediateDeath();
    }
  }

  private Boolean isRandomBuffPossible() {
    Random rnd = new Random();
    Integer number = rnd.nextInt(100) + 1;
    return (number < 25);
  }

  public Droid returnDroidOnCell(int currentX, int currentY) {
    return (Droid) map[currentX][currentY];
  }

  public Buff returnBuffOnCell(int currentX, int currentY) {
    return (Buff) map[currentX][currentY];
  }
}
