package models;

import java.util.Random;

public class RepairDroid extends Droid {

  private Integer healingPower;
  private Integer criticalHealingPower;
  private Integer healingRadius;

  public Integer getHealingPower() {
    return healingPower;
  }

  public void setHealingPower(Integer healingPower) {
    this.healingPower = healingPower;
  }

  public Integer getHealingRadius() {
    return healingRadius;
  }

  public void setHealingRadius(Integer healingRadius) {
    this.healingRadius = healingRadius;
  }

  public RepairDroid(Integer x, Integer y, Color color, Integer id) {
    maximumHealth = 150;
    currentHealth = 150;
    healingPower = 50;
    criticalHealingPower = 70;
    healingRadius = 2;
    walkRadius = 2;
    this.setX(x);
    this.setY(y);
    this.color = color;
    this.id = id;
  }

  public Integer getCriticalHealingPower() {
    return criticalHealingPower;
  }

  public void setCriticalHealingPower(Integer criticalHealingPower) {
    this.criticalHealingPower = criticalHealingPower;
  }

  /*
   * Implements healing process
   */
  public Boolean heal(Integer x, Integer y) {
    if (Arena.getInstance().isCordEmpty(x, y)) {
      System.out.println("There is nothing on this cell to heal.");
      return false;
    } else if (isDroidInHealingRange(x, y)) {
      if (isCriticalHeal()) {
        Arena.getInstance().returnDroidOnCell(x, y).setCurrentHealth(
            Arena.getInstance().returnDroidOnCell(x, y).getCurrentHealth() + this
                .getCriticalHealingPower());
      } else {
        Arena.getInstance().returnDroidOnCell(x, y).setCurrentHealth(
            Arena.getInstance().returnDroidOnCell(x, y).getCurrentHealth() + this
                .getHealingPower());
      }
      if (Arena.getInstance().returnDroidOnCell(x, y).isHealthMax()) {
        Arena.getInstance().returnDroidOnCell(x, y).
            setCurrentHealth(Arena.getInstance().returnDroidOnCell(x, y).getMaximumHealth());
      }
      System.out.println("Your droid has healing power: " + this.getHealingPower());
      System.out.println("Droid on [" + x + ", " + y + "] was healed and its health is:"
          + Arena.getInstance().returnDroidOnCell(x, y).getCurrentHealth() + " out of " + Arena
          .getInstance().returnDroidOnCell(x, y).getMaximumHealth());
      return true;
    } else {
      System.out.println("The enemy is too far");
      return false;
    }
  }

  /*
   * Checks if the droid can heal offered coordinates
   */
  private Boolean isDroidInHealingRange(Integer x, Integer y) {
    Integer distance = findDistanceBetweenCoords(x, y).intValue();
    return (distance <= healingRadius);
  }

  public void getInfoAboutDroid() {
    super.getInfoAboutDroid();
    System.out.println("Healing range: " + this.getHealingRadius());
    System.out.println("Healing damage: " + this.getHealingPower());
  }

  /*
   * Checks if heal is critical
   */
  public Boolean isCriticalHeal() {
    Random rnd = new Random();
    Integer chanceOfCriticalHeal = rnd.nextInt(100) + 1;
    if (chanceOfCriticalHeal < 8) {  //8 because of 7 percent of crit
      System.out.println("Critical heal! " + this.getCriticalHealingPower() + " instead of " + this
          .getHealingPower());
      return true;
    }
    return false;
  }
}
