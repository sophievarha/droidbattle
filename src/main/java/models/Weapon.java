package models;

import java.util.Random;

public class Weapon {

  private String name;
  private Integer radius;
  private Integer damage;
  private Integer criticalDamage;

  Weapon() {
    name = "Blaster";
    radius = 2;
    damage = 50;
    criticalDamage = 70;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getRadius() {
    return radius;
  }

  public void setRadius(Integer radius) {
    this.radius = radius;
  }

  public Integer getCriticalDamage() {
    return criticalDamage;
  }

  public void setCriticalDamage(Integer criticalDamage) {
    this.criticalDamage = criticalDamage;
  }

  /*
   * Returns critical damage instead of normal in 5%
   */
  public Integer getDamage() {
    return damage;

  }

  public void setDamage(Integer damage) {
    this.damage = damage;
  }

  /*
   * Checks if damage is critical
   */
  public Boolean isCriticalHit() {
    Random rnd = new Random();
    Integer chanceOfCriticalHit = rnd.nextInt(100) + 1;
    if (chanceOfCriticalHit < 75) {  //6 because of 5 percent of crit
      System.out
          .println("Critical hit! " + this.getCriticalDamage() + " instead of " + this.getDamage());
      return true;
    }
    return false;
  }
}