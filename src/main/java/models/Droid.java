package models;

public abstract class Droid {

  protected Integer id;
  protected Color color;
  protected Integer x;
  protected Integer y;
  protected Integer currentHealth;
  protected Integer maximumHealth;
  protected Integer walkRadius;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }

  public Integer getX() {
    return x;
  }

  public void setX(Integer x) {
    this.x = x;
  }

  public Integer getY() {
    return y;
  }

  public void setY(Integer y) {
    this.y = y;
  }

  public Integer getCurrentHealth() {
    return currentHealth;
  }

  public void setCurrentHealth(Integer currentHealth) {
    this.currentHealth = currentHealth;
  }

  public Integer getMaximumHealth() {
    return maximumHealth;
  }

  public void setMaximumHealth(Integer maximumHealth) {
    this.maximumHealth = maximumHealth;
  }

  public Integer getWalkRadius() {
    return walkRadius;
  }

  public void setWalkRadius(Integer walkRadius) {
    this.walkRadius = walkRadius;
  }

  public void stepOnBuff(Buff buff) {
    buff.getBuff(this);
  }

  /*
   * Checks if droid is still alive
   */
  public Boolean isAlive() {
    return currentHealth > 0;
  }

  /*
   * Checks if droid wasn't healed more than maxhp
   */
  public Boolean isHealthMax() {
    return currentHealth > maximumHealth;
  }

  /*
   * Checks if chosen droid is from your team
   */
  public Boolean isTeammate(Droid droid) {
    return (this.getColor() == droid.getColor());
  }

  /*
   * Checks if your own droid is chosen
   */
  public Boolean isTheSame(Integer x, Integer y) {
    return (this.getX() == x && this.getY() == y);
  }

  /*
   * Implements the walking process
   */
  public Boolean walk(Integer x, Integer y) {
    if ((Arena.getInstance().returnBuffOnCell(x, y) != null) && (Arena.getInstance().returnBuffOnCell(x, y).getClass() == ImmediateDeath.class)) {
      System.out.println("Oops! You stepped on a buff!");
      this.stepOnBuff(Arena.getInstance().returnBuffOnCell(x, y));
      Arena.getInstance().deleteBuff(x, y);
      return true;
    }
    if (Arena.getInstance().isCordEmpty(x, y) && checkWalkRange(x, y)) {
      System.out.println("Droid with id " + this.getId() +
          " on [" + this.getX() + ", " + this.getY() + "] moved to [" + x + " " + y + "].");
      Arena.getInstance().moveTo(this.getX(), this.getY(), x, y);
      this.setX(x);
      this.setY(y);
      return true;
    } else if (this.isTheSame(x, y)) {
      System.out.println("You are already standing here.");
      return false;
    } else if (!Arena.getInstance().isCordEmpty(x, y)) {
      System.out.println("This cell is occupied by another droid");
      return false;
    } else {
      System.out.println("You can't walk there, too far!");
      return false;
    }
  }

  /*
   * Checks if droid is not asked to move too far
   */
  private Boolean checkWalkRange(Integer x, Integer y) {
    Integer distance = findDistanceBetweenCoords(x, y).intValue();
    return (distance <= walkRadius);
  }

  /*
   * Calculates the distance between droid and offered position
   */
  protected Double findDistanceBetweenCoords(Integer x, Integer y) {
    return Math.sqrt(Math.pow((x - this.x), 2) + Math.pow((y - this.y), 2));
  }

  /*
   * Shows all the information about chosen droid
   */
  public void getInfoAboutDroid() {
    System.out.println("Id = " + this.getId());
    System.out.println("Color: " + this.getColor().toString());
    System.out.println("Coordinates are: x = " + this.getX() + ", y = " + this.getY());
    System.out.println("Current health: " + this.getCurrentHealth());
    System.out.println("Walking range: " + this.getWalkRadius());
  }
}
