package models;

public class ImmediateDeath extends Buff {
  public void getBuff(Droid droid) {
    Arena.getInstance().returnDroidOnCell(droid.getX(), droid.getY()).setCurrentHealth(
        droid.currentHealth - droid.maximumHealth);
    System.out.println("You stepped on the mine!");
    Arena.getInstance().deleteBuff(droid.getX(), droid.getY());
  }
}
