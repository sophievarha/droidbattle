package models;

public class Player {

  private String name;
  private Color color;

  public Player(Color color, String name) {
    this.setColor(color);
    this.setName(name);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }
}
