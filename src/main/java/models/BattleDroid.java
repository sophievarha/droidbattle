package models;

public class BattleDroid extends Droid {

  private Weapon weapon;

  public Weapon getWeapon() {
    return weapon;
  }

  public void setWeapon(Weapon weapon) {
    this.weapon = weapon;
  }

  BattleDroid(Integer x, Integer y, Color color, Integer id) {
    maximumHealth = 200;
    currentHealth = 200;
    weapon = new Weapon();
    walkRadius = 3;
    this.setX(x);
    this.setY(y);
    this.setColor(color);
    this.id = id;
  }

  /*
   * Implements the shooting process
   */
  public Boolean shoot(Integer x, Integer y) {
    if (Arena.getInstance().isCordEmpty(x, y)) {
      System.out.println("There is nothing on this cell to shoot.");
      return false;
    } else if (this.isTheSame(x, y) || this
        .isTeammate(Arena.getInstance().returnDroidOnCell(x, y))) {
      System.out.println("You can't shoot at yourself or your teammate!");
      return false;
    } else if (isEnemyInWeaponRange(x, y)) {
      if (this.weapon.isCriticalHit()) {
        Arena.getInstance().returnDroidOnCell(x, y).setCurrentHealth(Arena.getInstance().
            returnDroidOnCell(x, y).getCurrentHealth() - this.weapon.getCriticalDamage());
      } else {
        Arena.getInstance().returnDroidOnCell(x, y).setCurrentHealth(Arena.getInstance().
            returnDroidOnCell(x, y).getCurrentHealth() - this.weapon.getDamage());
      }
      if (!Arena.getInstance().returnDroidOnCell(x, y).isAlive()) {
        Arena.getInstance().deleteDroid(x, y);
        System.out.println("Droid on [" + x + ", " + y + "] died.");
      } else {
        System.out.println("Your droid wears weapon " + this.getWeapon().getName() +
            " which has damage: " + this.getWeapon().getDamage());
        System.out.println("Droid on [" + x + ", " + y + "] was shot and its health is: "
            + Arena.getInstance().returnDroidOnCell(x, y).getCurrentHealth() + " out of " + Arena
            .getInstance().returnDroidOnCell(x, y).getMaximumHealth());
      }
      return true;
    } else {
      System.out.println("The enemy is too far");
      return false;
    }
  }

  /*
   * Checks if the weapon can shoot at offered coordinates
   */
  private Boolean isEnemyInWeaponRange(Integer x, Integer y) {
    Integer distance = findDistanceBetweenCoords(x, y).intValue();
    return (distance <= weapon.getRadius());
  }

  /*
   * Shows all the information about chosen battledroid
   */
  public void getInfoAboutDroid() {
    super.getInfoAboutDroid();
    System.out.println("Weapon type: " + this.getWeapon().getName());
    System.out.println("Weapon's range: " + this.getWeapon().getRadius());
    System.out.println("Weapon's damage: " + this.getWeapon().getDamage());
  }
}
